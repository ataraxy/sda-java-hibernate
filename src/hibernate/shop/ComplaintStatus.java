package hibernate.shop;

public enum ComplaintStatus {
    PENDING, REJECTED, APPROVED
}
